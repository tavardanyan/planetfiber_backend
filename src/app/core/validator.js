import models from '../models';
import coreRepository from './repository';
import { ARRAY_TYPES, ISO_LANGS_OBJECT } from '../../data/TYPES';

export const regexValidation = {
  platform: new RegExp(`^${ARRAY_TYPES.platform.join('$|^')}$`),

  fileType: new RegExp(`^${ARRAY_TYPES.fileType.join('$|^')}$`),

  isoLangs: new RegExp(`^${Object.keys(ISO_LANGS_OBJECT).join('$|^')}$`),

};

const isResourceExist = async (where, modelName) => {
  const response = await coreRepository.findOne(where, {}, modelName);
  return !!response;
};

export const isExistArrayByCondition = async (where, repository, length) => {
  const response = await repository.findAll(where);
  if (response.length !== length) return false;
  return true;
};

export const errorMessage = {
  // Main function errors
  deletedResource: (val) => `The resource with ID: ${val} is deleted`,
  notDeletedResource: (val) => `The resource with ID: ${val} is not deleted`,
  fieldIsExists: (key, val) => `A field with ${key}: ${val} already exists`,
  fieldIsNotExists: (key, val) => `A field with ${key}: ${val} does not exist`,
  modelNotExists: (key, val) => `The ${key} with ID: ${val} does not exist`,

  fieldFromToInteger: (from, to) => `The field must be from ${from} to ${to} integer `,
  fieldFromToNumeric: (from, to) => `The field must be from ${from} to ${to} numeric `,
  fieldFromToString: (from, to) => `The field must be from ${from} to ${to} characters length `,

  fieldMinString: (min) => `The field must be min ${min} characters length `,
  fieldMaxString: (max) => `The field must be max ${max} characters length `,
  fieldMinInteger: (min) => `The field must be min ${min} integer `,
  fieldMaxInteger: (max) => `The field must be max ${max} integer `,

  fieldMustBeInteger: (val) => `The field must be ${val} integer `,
  fieldMustBeString: (val) => `The field must be ${val} characters length `,
  fieldMustBeOneOf: (val) => `The field must be one of ${val} `,

  // Main const errors
  fieldRequired: 'The field is required',
  isString: 'The field must be a String',
  isBoolean: 'The field must be boolean',
  includeExclude: 'The include and exclude fields cannot exist the same time',
  lineColorsRequired: 'The field must have color and line fields',
  isPositiveNumber: 'The field must be positive numbers',
  checkAbove: 'First solve the errors above',
  data: 'The date must be YYYY-MM-DD format',
  idDeleted: 'The resource is deleted',
  idNotDeleted: 'The resource is not deleted',
  isUnique: 'The field must be unique',
  colors: 'Type colors must be hexadecimal',
  colorTaken: 'The color with obj, type, size, line already exists',
  zoomKeys: 'The zoom must have to objects [from , to]',
  zoomFromTo: 'zoom.from must by greater than zoom to',
  array: 'The field must be array',
  phoneRegex: 'The value must be a valid phone number',
  email: 'Please enter a valid email address',
  queryString: 'The field must be a queryString',
  jsonError: 'The field must be a valid geometry object',
  isObject: 'The field must be a object',
  isBigInt: 'The field must be a positive bigint',
  // Auth
  userNameOrEmailRequired: 'One of username/email is required',
  isTokenRegex: 'The token must be with /[0-9a-zA-Z-\\_]/ pattern',

  // Role
  permissionMustBe: 'The permission value can be only digits',

  // User
  permissionAndRole: 'The permission and (roleId or departmentId) cannot exist the same time',

  // Mapping
  isIncludeItemString: 'The include items must be a string',
  includeItemsMustBeOneOf: (val) => `The include items must be one of ${val}`,
  isExcludeItemString: 'The exclude items must be a string',
  excludeItemsMustBeOneOf: (val) => `The exclude items must be one of ${val}`,

  // Migration
  isFileExists: (fileName) => `The file with fileName: ${fileName} dose not exists`,
};

export const coreValidator = {

  modelIdInQuery(modelName) {
    return {
      in: ['query'],
      errorMessage: (val) => errorMessage.modelNotExists(modelName, val),
      custom: { options: (id) => isResourceExist({ id }, modelName) },
    };
  },
  modelIdInParams(modelName) {
    return {
      in: ['params'],
      errorMessage: (val) => errorMessage.modelNotExists(modelName, val),
      custom: { options: (id) => isResourceExist({ id }, modelName) },
    };
  },
  modelNameInParams(modelName) {
    return {
      in: ['params'],
      errorMessage: (val) => errorMessage.modelNotExists(modelName, val),
      custom: { options: (name) => isResourceExist({ name }, modelName) },
    };
  },
  modelIdInBody(modelName) {
    return {
      in: ['body'],
      errorMessage: (val) => errorMessage.modelNotExists(modelName, val),
      custom: { options: (id) => isResourceExist({ id }, modelName) },
    };
  },
  isNotDeletedResource(modelName) {
    return {
      in: ['params'],
      errorMessage: (val) => errorMessage.notDeletedResource(val),
      custom: {
        errorMessage: errorMessage.idNotDeleted,
        async options(id) {
          const response = await coreRepository.findOne(
            { id: Number(id), deletedAt: { [models.Op.ne]: null } },
            { paranoid: false },
            modelName
          );
          return !!response;
        },
      },
    };
  },
  langInQuery: {
    in: ['query'],
    errorMessage: errorMessage.fieldRequired,
    isEmpty: { negated: true },
    matches: {
      options: [regexValidation.isoLangs],
      errorMessage: errorMessage.fieldMustBeOneOf(regexValidation.isoLangs),
    },
  },
  langInBody: {
    in: ['body'],
    errorMessage: errorMessage.fieldRequired,
    isEmpty: { negated: true },
    matches: {
      options: [regexValidation.isoLangs],
      errorMessage: errorMessage.fieldMustBeOneOf(regexValidation.isoLangs),
    },
  },
  fieldRequiredObject: {
    in: ['body'],
    errorMessage: errorMessage.fieldRequired,
    isEmpty: { negated: true },
    custom: {
      options(object) {
        if (typeof object !== 'object') {
          this.message = errorMessage.isObject;
          return false;
        }
        return true;
      }
    }
  },
  email: {
    in: ['body'],
    errorMessage: errorMessage.email,
    isEmail: true,
    isLength: {
      errorMessage: errorMessage.fieldFromToString(0, 255),
      options: { min: 0, max: 255 },
    },
  },
  name: {
    in: ['body'],
    errorMessage: errorMessage.fieldRequired,
    trim: true,
    isEmpty: { negated: true },
    isString: {
      errorMessage: errorMessage.isString,
      negated: false,
    },
    isLength: {
      errorMessage: errorMessage.fieldFromToString(2, 60),
      options: { min: 2, max: 60 },
    },
  },
  nameInQuery: {
    in: ['query'],
    errorMessage: errorMessage.fieldRequired,
    trim: true,
    isEmpty: { negated: true },
    isString: {
      errorMessage: errorMessage.isString,
      negated: false,
    },
    isLength: {
      errorMessage: errorMessage.fieldFromToString(2, 60),
      options: { min: 2, max: 60 },
    },
  },
  offset: {
    in: ['query'],
    errorMessage: errorMessage.fieldRequired,
    isEmpty: { negated: true },
    isInt: {
      options: { min: 0, max: 2147483640 },
      errorMessage: errorMessage.fieldFromToInteger(0, 2147483640),
    },
  },
  limit: {
    in: ['query'],
    errorMessage: errorMessage.fieldRequired,
    isEmpty: { negated: true },
    isInt: {
      errorMessage: errorMessage.fieldFromToInteger(0, 2147483640),
      options: { min: 0, max: 2147483640 },
    },
  },
  queryString: {
    in: ['query'],
    errorMessage: errorMessage.fieldRequired,
    matches: {
      options: [/^[a-zA-Z,]{1,}$/],
      errorMessage: errorMessage.queryString,
    },
  },
  fromToInt(from, to) {
    return {
      in: ['body'],
      errorMessage: errorMessage.fieldRequired,
      isEmpty: { negated: true },
      isInt: {
        options: { min: from, max: to },
        errorMessage: errorMessage.fieldFromToInteger(from, to),
      },
    };
  },
  fromToNumeric(from, to) {
    return {
      in: ['body'],
      errorMessage: errorMessage.fieldRequired,
      isEmpty: { negated: true },
      isDecimal: {
        options: { min: from, max: to },
        errorMessage: errorMessage.fieldFromToNumeric(from, to),
      },
    };
  },

  stringDefault: {
    in: ['body'],
    errorMessage: errorMessage.fieldRequired,
    trim: true,
    isEmpty: { negated: true },
    isString: {
      errorMessage: errorMessage.isString,
      negated: false,
    },
    isLength: {
      errorMessage: errorMessage.fieldFromToString(0, 255),
      options: { min: 0, max: 255 },
    },
  },
  stringDefaultInQuery: {
    in: ['query'],
    errorMessage: errorMessage.fieldRequired,
    isEmpty: { negated: true },
    trim: true,
    isString: {
      errorMessage: errorMessage.isString,
      negated: false,
    },
    isLength: {
      errorMessage: errorMessage.fieldFromToString(0, 255),
      options: { min: 0, max: 255 },
    },
  },
  boolean: {
    in: ['body'],
    errorMessage: errorMessage.isBoolean,
    isBoolean: true,
  },
  Numeric: {
    in: ['body'],
    errorMessage: errorMessage.fieldRequired,
    isEmpty: { negated: true },
    isDecimal: {
      options: { min: -2147483640, max: 2147483640 },
      errorMessage: errorMessage.fieldFromToNumeric(-2147483640, 2147483647),
    },
  },
  positiveNumeric: {
    in: ['body'],
    errorMessage: errorMessage.fieldRequired,
    isEmpty: { negated: true },
    isDecimal: {
      options: { min: 0, max: 2147483640 },
      errorMessage: errorMessage.fieldFromToNumeric(0, 2147483647),
    },
  },
  positiveBigint: {
    in: ['body'],
    errorMessage: errorMessage.fieldRequired,
    isEmpty: { negated: true },
    custom: {
      options(value) {
        value = BigInt(value);
        if (typeof value !== 'bigint' || value < 0n) {
          this.message = errorMessage.isBigInt;
          return false;
        }
        return true;
      }
    }
  },
  phone: {
    in: ['body'],
    errorMessage: errorMessage.fieldRequired,
    isEmpty: { negated: true },
    matches: {
      errorMessage: errorMessage.phoneRegex,
      options: [/^[+]?[0-9]{9,20}$/],
    },
  },
  fileIdInBody: {
    in: ['body'],
    errorMessage: (val) => errorMessage.modelNotExists('file', val),
    custom: {
      options: (id) => isResourceExist({ where: { id, type: 'FILE' } }, 'file'),
    },
  },
  fileIdInBodyNullable: {
    in: ['body'],
    errorMessage: (val) => errorMessage.modelNotExists('file', val),
    custom: {
      options: (id) => id
        ? isResourceExist({ where: { id, type: 'FILE' } }, 'file')
        : true,
    },
  },
  folderInBody: {
    in: ['body'],
    errorMessage: (val) => errorMessage.modelNotExists('folder', val),
    custom: {
      options: (id) => isResourceExist({ id, type: 'FOLDER' }, 'file'),
    },
  },
  fileArrayInBody: {
    in: ['body'],
    isArray: { errorMessage: errorMessage.array },
    errorMessage: (val) => `The files with IDs: ${val} do not exist`,
    custom: {
      options: (ids) => isExistArrayByCondition(
        { where: { id: { $in: ids }, type: 'FILE' } },
        models.file,
        ids.length
      ),
    },
  },
};
