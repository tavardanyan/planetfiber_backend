module.exports = (sequelize, DataTypes) => {
  const brand = sequelize.define('brand', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    shortName: DataTypes.STRING,
    address: DataTypes.STRING,
    phone: DataTypes.STRING,
    email: DataTypes.STRING,
    description: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'brands',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['deletedAt'] },
    },
    hooks: {
      afterSave: async (n) => {
        // eslint-disable-next-line no-param-reassign
        if (n && n.dataValues) ['deletedAt'].forEach((prop) => delete n.dataValues[prop]);
      },
    },
  });
  brand.associate = function associate(models) {
    models.brand.belongsTo(models.country, { foreignKey: { allowNull: false } });
    models.brand.belongsTo(models.file, { as: 'img' });
    models.brand.belongsTo(models.user, { foreignKey: 'createdBy' });
    models.brand.belongsTo(models.user, { foreignKey: 'updatedBy' });
  };
  return brand;
};
