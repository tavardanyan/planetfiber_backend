import { ARRAY_TYPES } from '../../data/TYPES';

module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      unique: true,
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    address: DataTypes.STRING,
    birthday: DataTypes.DATEONLY,
    gender: DataTypes.ENUM(ARRAY_TYPES.gender),
    email: DataTypes.STRING,
    permission: DataTypes.STRING,
    isVerified: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    isBlocked: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'users',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['deletedAt', 'password'] },
    },
    hooks: {
      afterSave: async (n) => {
        // eslint-disable-next-line no-param-reassign
        if (n && n.dataValues) ['deletedAt', 'password'].forEach((prop) => delete n.dataValues[prop]);
      },
    },
  });
  user.associate = function associate(models) {
    models.user.belongsTo(models.country);
    models.user.belongsTo(models.user, { foreignKey: 'createdBy' });
    models.user.belongsTo(models.user, { foreignKey: 'updatedBy' });
  };
  return user;
};
