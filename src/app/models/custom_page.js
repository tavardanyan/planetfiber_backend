module.exports = (sequelize, DataTypes) => {
  const customPage = sequelize.define('custom_page', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    title_en: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    title_ru: DataTypes.STRING,
    title_hy: DataTypes.STRING,
    text_en: DataTypes.TEXT('long'),
    text_ru: DataTypes.TEXT('long'),
    text_hy: DataTypes.TEXT('long'),
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'custom_pages',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['deletedAt'] },
    },
    hooks: {
      afterSave: async (n) => {
        // eslint-disable-next-line no-param-reassign
        if (n && n.dataValues) ['deletedAt'].forEach((prop) => delete n.dataValues[prop]);
      },
    },
  });
  customPage.associate = function associate(models) {
    models.custom_page.belongsTo(models.user, { foreignKey: 'createdBy' });
    models.custom_page.belongsTo(models.user, { foreignKey: 'updatedBy' });
  };
  return customPage;
};
