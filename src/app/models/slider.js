module.exports = (sequelize, DataTypes) => {
  const slider = sequelize.define('slider', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    title_en: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    title_ru: DataTypes.STRING,
    title_hy: DataTypes.STRING,
    url: DataTypes.STRING,
    type: DataTypes.ENUM('PRODUCT', 'BANNER', 'SECTION'),
    position: {
      type: DataTypes.INTEGER,
      defaultValue: 50,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'sliders',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['deletedAt'] },
    },
    hooks: {
      afterSave: async (n) => {
        // eslint-disable-next-line no-param-reassign
        if (n && n.dataValues) ['deletedAt'].forEach((prop) => delete n.dataValues[prop]);
      },
    },
  });
  slider.associate = function associate(models) {
    models.slider.belongsTo(models.file);
    models.slider.belongsTo(models.user, { foreignKey: 'createdBy' });
    models.slider.belongsTo(models.user, { foreignKey: 'updatedBy' });
  };
  return slider;
};
