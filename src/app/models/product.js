module.exports = (sequelize, DataTypes) => {
  const product = sequelize.define('product', {
    title_en: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    title_ru: DataTypes.STRING,
    title_hy: DataTypes.STRING,
    description1_en: DataTypes.TEXT('long'),
    description1_ru: DataTypes.TEXT('long'),
    description1_hy: DataTypes.TEXT('long'),
    description2_en: DataTypes.TEXT('long'),
    description2_ru: DataTypes.TEXT('long'),
    description2_hy: DataTypes.TEXT('long'),
    details_en: DataTypes.TEXT('long'),
    details_ru: DataTypes.TEXT('long'),
    details_hy: DataTypes.TEXT('long'),
    key: DataTypes.STRING,
    armsoftId: DataTypes.STRING,
    model: DataTypes.STRING,
    price: DataTypes.DECIMAL(11, 8),
    color: DataTypes.STRING,
    availability: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'products',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['deletedAt'] },
    },
    hooks: {
      afterSave: async (n) => {
        // eslint-disable-next-line no-param-reassign
        if (n && n.dataValues) ['deletedAt'].forEach((prop) => delete n.dataValues[prop]);
      },
    },
  });
  product.associate = function associate(models) {
    models.product.belongsToMany(models.section, { through: 'product_sections' });
    models.product.belongsToMany(models.category, { through: 'product_categories' });
    models.product.belongsToMany(models.attribute, { through: models.product_attribute });
    models.product.belongsTo(models.partner);
    models.product.belongsTo(models.country);
    models.product.hasMany(models.file, { foreignKey: 'productId' });
    models.product.belongsTo(models.user, { foreignKey: 'createdBy' });
    models.product.belongsTo(models.user, { foreignKey: 'updatedBy' });
  };
  return product;
};
