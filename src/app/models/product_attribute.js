module.exports = (sequelize, DataTypes) =>
  sequelize.define(
    'product_attribute',
    {
      value: DataTypes.JSON,
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.fn('now'),
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.fn('now'),
      },
    },
    {
      tableName: 'product_attributes',
      underscored: true,
    }
  );
