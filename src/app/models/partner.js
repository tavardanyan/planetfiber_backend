module.exports = (sequelize, DataTypes) => {
  const partner = sequelize.define('partner', {
    name_en: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    name_ru: DataTypes.STRING,
    name_hy: DataTypes.STRING,
    description_en: DataTypes.STRING,
    description_ru: DataTypes.STRING,
    description_hy: DataTypes.STRING,
    email: DataTypes.STRING,
    address: DataTypes.STRING,
    phone: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'partners',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['deletedAt'] },
    },
    hooks: {
      afterSave: async (n) => {
        // eslint-disable-next-line no-param-reassign
        if (n && n.dataValues) ['deletedAt'].forEach((prop) => delete n.dataValues[prop]);
      },
    },
  });
  partner.associate = function associate(models) {
    models.partner.belongsTo(models.country);
    models.partner.belongsTo(models.user, { foreignKey: 'createdBy' });
    models.partner.belongsTo(models.user, { foreignKey: 'updatedBy' });
  };
  return partner;
};
