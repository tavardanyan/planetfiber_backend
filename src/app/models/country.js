module.exports = (sequelize, DataTypes) => {
  const country = sequelize.define('country', {
    enabled: DataTypes.BOOLEAN,
    code3l: DataTypes.STRING,
    code2l: DataTypes.STRING,
    name: DataTypes.STRING,
    nameOfficial: DataTypes.STRING,
    latitude: {
      type: DataTypes.DECIMAL(11, 8),
      get() {
        const rawValue = this.getDataValue('size');
        return rawValue && Number(rawValue);
      }
    },
    longitude: {
      type: DataTypes.DECIMAL(11, 8),
      get() {
        const rawValue = this.getDataValue('size');
        return rawValue && Number(rawValue);
      }
    },
    zoom: DataTypes.INTEGER,
    phoneCode: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'countries',
    underscored: true,
  });
  return country;
};
