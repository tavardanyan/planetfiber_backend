export function onCreateUserData(req, res, next) {
  try {
    const { body, userId } = req;
    body.createdBy = userId;
    body.updatedBy = userId;
    return next();
  } catch (error) {
    return next(error);
  }
}

export function onUpdateUserData(req, res, next) {
  try {
    const { body, userId } = req;
    body.updatedBy = userId;
    return next();
  } catch (error) {
    return next(error);
  }
}
