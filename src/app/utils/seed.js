const util = require('util');
const exec = util.promisify(require('child_process').exec);

export async function seedRun(fileName) {
  await exec(`sequelize db:seed --seed ${fileName}`);
}

export async function seedUndo(fileName) {
  await exec(`sequelize db:seed:undo --seed ${fileName}`);
}

export async function seedAll() {
  await exec('sequelize-cli db:seed:all');
}
