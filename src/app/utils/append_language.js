import { ForbiddenError } from './error_handler';
import { get as redisGet } from './redis';

export default async function appendLanguage(req, _res, next) {
  try {
    const languageCode = 'en';
    // const languageCode = (req.headers['accept-language'] || 'en').slice(0, 2);

    const redisData = await redisGet(languageCode);

    req.language = redisData.code;
    return next();
  } catch (err) {
    return next(new ForbiddenError('Wrong language code'));
  }
}
