/* eslint-disable import/prefer-default-export */
import { post } from 'axios';

export async function slackRequest(chanel, err) {
  const url = 'https://slack.com/api/chat.postMessage';
  await post(url, {
    channel: `#${chanel}`,
    text: err,
  }, { headers: { authorization: `Bearer ${process.env.SLACK_TOKEN}` } });
}
