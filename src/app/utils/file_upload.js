import sharp from 'sharp';

import extTypes from '../../../extensionTypes.json';
import { ValidationError } from './error_handler';

const supportedImageExtensionList = [
  'jpg',
  'jpeg',
  'png',
  'gif',
  'bmp',
  'svg',
  'svg+xml',
];
const maxFileLengthInBytes = 20 * 1024 * 1024;

export async function fileUpload(req, res, next) {
  try {
    console.log(req.files, 'eeeeeeeee');
    if (!req.files) {
      const massage = 'Validation failed';
      const errors = { file: { msg: 'File not found.' } };
      return next(new ValidationError(massage, errors));
    }

    const files = Object.values(req.files);
    if (files.length === 0 || !files[0].data) {
      const massage = 'Validation failed';
      const errors = { file: { msg: 'File not found.' } };
      return next(new ValidationError(massage, errors));
    }

    if (files.length > 1 || files[0].length > 0) {
      const massage = 'Validation failed';
      const errors = { file: { msg: 'A single file is allowed per request.' } };
      return next(new ValidationError(massage, errors));
    }

    if (files[0].data.length > maxFileLengthInBytes) {
      const massage = 'Validation failed';
      const errors = {
        file: { msg: 'File size exceeds the maximum allowed size.' },
      };
      return next(new ValidationError(massage, errors));
    }

    let format = files[0].mimetype;
    if (!format) {
      const massage = 'Validation failed';
      const errors = { file: { msg: 'File extension application not found' } };
      return next(new ValidationError(massage, errors));
    }
    format = extTypes[format.toLowerCase()];
    if (!format) {
      const massage = 'Validation failed';
      const errors = { file: { msg: 'File extension is not supported.' } };
      return next(new ValidationError(massage, errors));
    }
    if (supportedImageExtensionList.includes(format)) {
      console.log('hmmmmmmmmmmm', files[0].data);
      // eslint-disable-next-line no-shadow
      const { format, width, height } = await sharp(files[0].data).metadata();
      console.log('hehee');
      req.fileExtension = { format, width, height };
    } else {
      req.fileExtension = { format };
    }
    [req.file] = files;
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function multipleMediaUpload(req, res, next) {
  try {
    if (!req.files || !req.files.media) {
      return next(new ValidationError('No files found.'));
    }
    let mediaArrays = [];
    if (req.files.media && !req.files.media.length)
      mediaArrays.push(req.files.media);
    else if (req.files.media) mediaArrays = Object.values(req.files.media);

    if (mediaArrays.length > 8) {
      const massage = 'Validation failed';
      const errors = {
        file: { msg: 'Maximum 8 media files are allowed per req.' },
      };
      return next(new ValidationError(massage, errors));
    }
    req.extensions = [];
    req.files = [];

    // eslint-disable-next-line no-restricted-syntax
    for await (const file of mediaArrays) {
      if (file.data.length > maxFileLengthInBytes) {
        const massage = 'Validation failed';
        const errors = {
          file: { msg: 'Maximum 8 media files are allowed per req.' },
        };
        return next(new ValidationError(massage, errors));
      }
      let extension = file.mimetype;
      if (!extension) {
        const massage = 'Validation failed';
        const errors = {
          file: { msg: 'File extension application not found' },
        };
        return next(new ValidationError(massage, errors));
      }
      extension = extTypes[extension.toLowerCase()];
      if (!extension) {
        const massage = 'Validation failed';
        const errors = { file: { msg: 'File extension is not supported.' } };
        return next(new ValidationError(massage, errors));
      }
      if (supportedImageExtensionList.includes(extension)) {
        const { format, width, height } = await sharp(file.data).metadata();
        req.extensions.push({ format, width, height });
      } else {
        const massage = 'Validation failed';
        const errors = { file: { msg: 'File extension is not supported.' } };
        return next(new ValidationError(massage, errors));
      }
      req.files.push(file);
    }
    return next();
  } catch (error) {
    return next(error);
  }
}
