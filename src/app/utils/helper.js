export function destructQuery(query, include) {
  const options = {
    offset: query.offset || 0,
    attributes: {},
    include,
  };
  if (parseInt(query.limit, 10) !== 0) options.limit = query.limit || 20;

  if (query.include) {
    options.attributes = query.include
      .split(',')
      .filter((field) => options.include.indexOf(field) === -1);
    options.include = options.include.filter(
      (field) => query.include.split(',').indexOf(field) !== -1
    );
  }
  if (query.exclude) {
    options.include = options.include.filter(
      (field) => query.exclude.split(',').indexOf(field) === -1
    );
    options.attributes.exclude = query.exclude.split(',');
  }
  return options;
}

// first see color service ( create, update, upsert ) logic then change this
export function createColorAndLibDataGenerator(data, objectType, libType) {
  const {
    additionalData: { zoom, ...additionalData },
    ...restData
  } = data;

  additionalData.objectType = objectType;

  const toCreateData = {
    ...restData,
    additionalData,
  };

  const libData = {
    libType,
    zoomFrom: zoom.from,
    zoomTo: zoom.to,
  };

  return { toCreateData, libData };
}

// first see color service ( create, update, upsert ) logic then change this
export function updateColorAndLibDataGenerator(data, id, libType) {
  const {
    additionalData: {
      zoom: { from: zoomFrom, to: zoomTo } = {},
      ...additionalData
    } = {},
    ...restData
  } = data;

  const toUpdateData = {
    ...restData,
  };

  let libData = {};
  const libWhere = { libType, libTypeId: parseInt(id, 10) };
  if (zoomFrom && zoomTo) {
    libData = { zoomFrom, zoomTo };
  }

  return {
    toUpdateData,
    additionalData,
    libData,
    libWhere,
  };
}

export function removeColorAndLibWhereGenerator(removed, libType) {
  const libWhere = {
    libType,
    libTypeId: removed.id,
  };

  return libWhere;
}

export function revertColorDataAndLibWhereGenerator(reverted, libType) {
  const libWhere = {
    libType,
    libTypeId: reverted.id,
  };
  return libWhere;
}

export function listToTree(dataset, childrenName) {
  const hashTable = {};
  dataset.forEach((aData) => {
    hashTable[aData.id] = aData;
  });
  const dataTree = [];
  dataset.forEach((aData) => {
    if (aData.parentId) {
      if (hashTable[aData.parentId][childrenName])
        hashTable[aData.parentId][childrenName].push(hashTable[aData.id]);
      else hashTable[aData.parentId][childrenName] = [hashTable[aData.id]];
    } else {
      dataTree.push(hashTable[aData.id]);
    }
  });
  return dataTree;
}
