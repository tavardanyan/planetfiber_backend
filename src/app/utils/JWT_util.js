import { sign, verify } from 'jsonwebtoken';
import { get as redisGet } from './redis';

export function generateAccessToken(id) {
  return sign({ id }, process.env.JWT_SECRET);
}

export async function authenticateToken(req, res, next) {
  try {
    const authHeader = req.headers.authorization;
    const sessionId = req.headers['x-app-session'];
    const token = authHeader && authHeader.split(' ')[1];
    if (token == null) return res.sendStatus(401);

    const { id: userId } = await verify(token, process.env.JWT_SECRET, {});
    const redisData = await redisGet(userId);
    if (!redisData) return res.sendStatus(401);
    const isSessionExists = redisData.sessions.find(
      (item) => item.sessionId === sessionId
    );
    if (!isSessionExists) return res.sendStatus(401);

    const { user } = redisData;
    const { role, department } = req;

    req.userId = userId;
    req.userPermission = BigInt(user.permission);
    req.rolePermission = BigInt(role ? role.permission : 0);
    req.departmentPermission = BigInt(department ? department.permission : 0);
    return next();
  } catch (err) {
    return res.sendStatus(403);
  }
}
