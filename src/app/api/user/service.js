import uuid from 'uuid/v4';
import coreService from '../../core/service';
import repository from './repository';
import { ServiceError } from '../../utils/error_handler';
import { destructQuery } from '../../utils/helper';
import { createHash } from '../../utils/crypto_util';

const service = Object.create(coreService);

export default Object.assign(service, {
  /**
   *  Get All available resources
   *  @return {Array} array of resource ORM objects
   */
  getAll(query) {
    try {
      const where = { };
      const options = destructQuery(query, ['role', 'profilePicture', 'country', 'department']);
      return repository.findAll(where, options);
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Get One available resources
   *  @param {Number} pk : resource ID
   *  @return {Object} resource ORM object
   */
  getByPk(pk) {
    try {
      return repository.findByPk(pk, { include: ['country', 'profilePicture', 'role', 'department'] });
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Create resources
   *  @param {Object} data : resource data object
   *  @return {Object} resource ORM object
   */
  async store(data) {
    try {
      const id = uuid();
      const transaction = await repository.startTransaction();
      const created = await repository.create({
        id,
        ...data,
      }, { transaction });

      const role = await created.getRole({ transaction });
      const department = await created.getDepartment({ transaction });

      const permission = BigInt(role ? role.permission : 0)
      | BigInt(department ? department.permission : 0)
      | BigInt(created.permission || 0);

      const hashPassword = createHash(data.password, created.id);

      await repository.update({
        password: hashPassword,
        permission
      },
      { id: created.id },
      { transaction });
      created.permission = permission;
      created.setDataValue('role', role);
      created.setDataValue('department', department);
      created.setDataValue('profilePicture', await created.getProfilePicture({ transaction }));
      created.setDataValue('country', await created.getCountry({ transaction }));
      await repository.commitTransaction(transaction);
      return created;
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Update resources
   *  @param {Object} data : resource data object
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  async update(data, id) {
    try {
      if (data.password) {
        // eslint-disable-next-line no-param-reassign
        data.password = createHash(data.password, id);
      }

      if (data.roleId || data.departmentId || data.permission || data.username) {
        const prevUser = await repository.findByPk(id, { include: ['department', 'role'] });
        const { username: prevUsername, role: prevRole, department: prevDepartment } = prevUser;
        if (prevUsername === 'admin') {
          throw new ServiceError({
            name: 'user service',
            message: 'immutable user (permission, role, department, username)'
          });
        }
        if (data.roleId || data.departmentId) {
          let rolePermission = BigInt(prevRole ? prevRole.permission : 0);
          let departmentPermission = BigInt(prevDepartment ? prevDepartment.permission : 0);

          if (data.roleId) {
            const role = await repository.findByPk(data.roleId, { attributes: ['permission'] }, 'role');
            rolePermission = BigInt(role.permission);
          }
          if (data.departmentId) {
            const department = await repository.findByPk(data.departmentId, { attributes: ['permission'] }, 'department');
            departmentPermission = BigInt(department.permission);
          }
          data.permission = `${rolePermission | departmentPermission}`;
        }
      }
      await repository.update(
        data,
        { id },
      );
      return repository.findByPk(id, { include: ['country', 'department', 'profilePicture', 'role'] });
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Remove resources
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  remove(data, id) {
    try {
      return repository.softDeleteWithUserId(data, { id });
    } catch (error) {
      throw new ServiceError();
    }
  },

  /**
   *  Revert resources
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  async revert(data, id) {
    try {
      await repository.revertWithUserId(data, { id });
      return repository.findByPk(id, { include: ['country', 'department', 'profilePicture', 'role'] });
    } catch (error) {
      throw new ServiceError();
    }
  },
});
