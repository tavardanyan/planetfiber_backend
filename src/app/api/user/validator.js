import { checkSchema } from 'express-validator/check';
import { coreValidator, errorMessage } from '../../core/validator';
import repository from './repository';
import countryRepository from '../country/repository';

const validator = Object.create(coreValidator);

const validationFields = {
  username: {
    in: ['body'],
    errorMessage: errorMessage.fieldRequired,
    trim: true,
    isEmpty: { negated: true },
    isLength: {
      errorMessage: errorMessage.fieldFromToString(2, 20),
      options: { min: 2, max: 20 },
    },
    custom: {
      errorMessage: (val) => errorMessage.fieldIsExists('username', val),
      async options(username) {
        const response = await repository.findOne(
          { username },
          { paranoid: false },
        );
        return !response;
      },
    },
  },

  updateUsername: {
    in: ['body'],
    errorMessage: errorMessage.fieldRequired,
    trim: true,
    isLength: {
      errorMessage: errorMessage.fieldFromToString(2, 20),
      options: { min: 2, max: 20 },
    },
    custom: {
      errorMessage: (val) => errorMessage.fieldIsExists('username', val),
      async options(username, req) {
        const response = await repository.findOne(
          { username },
          { paranoid: false },
        );
        if (response && response.dataValues.id === Number(req.req.params.id))
          return true;
        return !response;
      },
    },
  },

  password: {
    in: ['body'],
    errorMessage: errorMessage.fieldRequired,
    trim: true,
    isEmpty: { negated: true },
    isLength: {
      errorMessage: errorMessage.fieldFromToString(6, 255),
      options: { min: 6, max: 255 },
    },
  },

  day: {
    in: ['body'],
    matches: {
      errorMessage: errorMessage.data,
      options: [/^\d{4}-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$/],
    },
  },

  gender: {
    in: ['body'],
    matches: {
      options: [/\b(?:MALE|FEMALE|NA)\b/],
      errorMessage: errorMessage.fieldMustBeOneOf(/\b(?:MALE|FEMALE|NA)\b/),
    },
  },

  include: {
    ...coreValidator.queryString,
    custom: {
      errorMessage: errorMessage.includeExclude,
      options: (include, { req }) => {
        if (req.query.exclude) return false;
        return true;
      },
    },
  },
  exclude: {
    ...coreValidator.queryString,
    custom: {
      errorMessage: errorMessage.includeExclude,
      options: (exclude, { req }) => {
        if (req.query.include) return false;
        return true;
      },
    },
  },
};

export default Object.assign(validator, {
  store: checkSchema({
    firstName: coreValidator.stringDefault,
    lastName: coreValidator.stringDefault,
    username: validationFields.username,
    password: validationFields.password,
    phone: coreValidator.phone,
    address: { ...coreValidator.stringDefault, optional: true },
    birthday: { ...validationFields.day, optional: true },
    gender: { ...validationFields.gender, optional: true },
    email: { ...coreValidator.stringDefault, optional: true },
    countryId: { ...coreValidator.modelIdInBody(countryRepository.modelName), optional: true },
    profilePictureId: { ...coreValidator.fileIdInBody, optional: true },
  }),

  update: checkSchema({
    id: coreValidator.modelIdInParams(repository.modelName),
    firstName: { ...coreValidator.stringDefault, optional: true },
    lastName: { ...coreValidator.stringDefault, optional: true },
    username: { ...validationFields.updateUsername, optional: true },
    password: { ...validationFields.password, optional: true },
    phone: { ...coreValidator.phone, optional: true },
    address: { ...coreValidator.stringDefault, optional: true },
    birthday: { ...validationFields.day, optional: true },
    gender: { ...validationFields.gender, optional: true },
    email: { ...coreValidator.stringDefault, optional: true },
    isVerified: { ...coreValidator.boolean, optional: true },
    isBlocked: { ...coreValidator.boolean, optional: true },
    countryId: { ...coreValidator.modelIdInBody(countryRepository.modelName), optional: true },
    profilePictureId: { ...coreValidator.fileIdInBody, optional: true },
  }),

  isExist: checkSchema({
    id: coreValidator.modelIdInParams(repository.modelName),
  }),

  isDeleted: checkSchema({
    id: coreValidator.isNotDeletedResource(repository.modelName),
  }),

  getAll: checkSchema({
    offset: { ...coreValidator.offset, optional: true },
    limit: { ...coreValidator.limit, optional: true },
    include: { ...validationFields.include, optional: true },
    exclude: { ...validationFields.exclude, optional: true },
  }),
});
