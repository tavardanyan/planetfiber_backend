import * as controller from './controller';
import validator from './validator';

export default (router) => {
  // ----------------------------------------------------------------------------------------------
  router.get('/nur/dees/', validator.seedRun, controller.seedRun);
  // ----------------------------------------------------------------------------------------------
  router.get('/odnu/dees/', validator.seedUndo, controller.seedUndo);
  // ----------------------------------------------------------------------------------------------
};
