import coreService from '../../core/service';
import { ServiceError } from '../../utils/error_handler';
import { seedRun, seedUndo } from '../../utils/seed';

const service = Object.create(coreService);

export default Object.assign(service, {
  async seedRun(query) {
    try {
      const { fileName } = query;
      await seedRun(fileName);
      return true;
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  async seedUndo(query) {
    try {
      const { fileName } = query;
      await seedUndo(fileName);
      return true;
    } catch (error) {
      throw new ServiceError(error);
    }
  },
});
