import service from './service';
import cw from '../../core/controller';

export const seedRun = cw((req) => service.seedRun(req.query));

export const seedUndo = cw((req) => service.seedUndo(req.query));
