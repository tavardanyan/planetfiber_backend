import coreService from '../../core/service';
import repository from './repository';
import { ServiceError } from '../../utils/error_handler';
import models from '../../models';
import { operatorsAliases as Op } from '../../../../sequelizeConfig';

const service = Object.create(coreService);

export default Object.assign(service, {
  /**
   *  Get All available resources
   *  @return {Array} array of resource ORM objects
   */
  async getAll(query, language) {
    try {
      const where = {};
      if (query.search)
        where[`title_${language}`] = { [Op.$iLike]: `%${query.search}%` };
      const attributesQuery = {};
      if (query.attributeIds) {
        attributesQuery.id = { $in: query.attributeIds.split(',') };
      }
      const categoriesQuery = {};
      if (query.categoryIds) {
        categoriesQuery.id = { $in: query.categoryIds.split(',') };
      }
      const options = {
        order: [
          ['id', 'ASC'],
          [`title_${language}`, 'ASC'],
        ],
        attributes: ['id', [`title_${language}`, 'title'], 'key', 'price', 'availability'],
        include: [
          {
            model: models.file,
            attributes: ['id', 'path', 'extension', 'dimensions', 'isMain'],
            limit: 1,
          },
          {
            model: models.attribute,
            where: attributesQuery,
            attributes: ['id', [`description_${language}`, 'description']],
          },
          {
            model: models.category,
            where: categoriesQuery,
            attributes: ['id', [`title_${language}`, 'title']],
          },
        ],
        offset: Number(query.offset) || 0,
        limit: Number(query.limit) || 20,
      };

      return repository.findAll(where, options);
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Get One available resources
   *  @param {Number} pk : resource ID
   *  @return {Object} resource ORM object
   */
  getByPk(pk, language) {
    try {
      return repository.findByPk(pk, {
        attributes: [
          'id',
          [`title_${language}`, 'title'],
          [`description1_${language}`, 'description1'],
          [`description2_${language}`, 'description2'],
          [`details_${language}`, 'details'],
          'key',
          ['armsoft_id', 'armsoftId'],
          'model',
          'price',
          'color',
          'availability',
        ],
        include: [
          {
            model: models.partner,
            attributes: ['id', [`name_${language}`, 'name']],
          },
          {
            model: models.category,
            attributes: ['id', 'key', [`title_${language}`, 'title']],
          },
          {
            model: models.country,
            attributes: ['id', 'name'],
          },
          {
            model: models.file,
            attributes: ['id', 'path', 'name', 'extension', 'dimensions', 'isMain'],
          },
          {
            model: models.attribute,
            attributes: ['id', [`description_${language}`, 'description']],
          },
        ],
      });
    } catch (error) {
      throw new ServiceError(error);
    }
  },
});
