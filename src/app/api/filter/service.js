import coreService from '../../core/service';
import repository from './repository';
import { ServiceError } from '../../utils/error_handler';
import { destructQuery } from '../../utils/helper';
import { operatorsAliases as Op } from '../../../../sequelizeConfig';

const service = Object.create(coreService);

export default Object.assign(service, {
  /**
   *  Get All available resources
   *  @return {Array} array of resource ORM objects
   */
  getAll(query) {
    try {
      const where = { };
      if (query.search) where.name = { [Op.$iLike]: `%${query.search}%` };
      const options = destructQuery(query, ['img']);
      return repository.findAll(where, options);
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Get One available resources
   *  @param {Number} pk : resource ID
   *  @return {Object} resource ORM object
   */
  getByPk(pk) {
    try {
      return repository.findByPk(pk, { include: ['country', 'img'] });
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Create resources
   *  @param {Object} data : resource data object
   *  @return {Object} resource ORM object
   */
  // async store(data) {
  //   try {
  //     const created = await repository.create({
  //       ...data,
  //     });
  //     created.setDataValue('country', await created.getCountry());
  //     created.setDataValue('img', await created.getImg());
  //     return created;
  //   } catch (error) {
  //     throw new ServiceError(error);
  //   }
  // },

  /**
   *  Update resources
   *  @param {Object} data : resource data object
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  // async update(data, id) {
  //   try {
  //     await repository.update(
  //       data,
  //       { id },
  //     );
  //     return repository.findByPk(id, { include: ['country', 'img'] });
  //   } catch (error) {
  //     throw new ServiceError(error);
  //   }
  // },

  /**
   *  Remove resources
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  // remove(data, id) {
  //   try {
  //     return repository.softDeleteWithUserId(data, { id });
  //   } catch (error) {
  //     throw new ServiceError();
  //   }
  // },

  /**
   *  Revert resources
   *  @param {Object} id : resource ID
   *  @return {Object} resource ORM object
   */
  // async revert(data, id) {
  //   try {
  //     await repository.revertWithUserId(data, { id });
  //     return repository.findByPk(id, { include: ['country', 'img'] });
  //   } catch (error) {
  //     throw new ServiceError();
  //   }
  // },
});
