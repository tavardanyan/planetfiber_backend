import { checkSchema } from 'express-validator/check';
import { coreValidator, errorMessage } from '../../core/validator';
import repository from './repository';

const validator = Object.create(coreValidator);

const validationFields = {
  include: {
    ...coreValidator.queryString,
    custom: {
      errorMessage: errorMessage.includeExclude,
      options: (include, { req }) => {
        if (req.query.exclude) return false;
        return true;
      },
    },
  },
  exclude: {
    ...coreValidator.queryString,
    custom: {
      errorMessage: errorMessage.includeExclude,
      options: (exclude, { req }) => {
        if (req.query.include) return false;
        return true;
      },
    },
  },
};

export default Object.assign(validator, {
  // store: checkSchema({
  //   name: coreValidator.name,
  //   shortName: { ...coreValidator.name, optional: true },
  //   address: { ...coreValidator.stringDefault, optional: true },
  //   phone: { ...coreValidator.phone, optional: true },
  //   email: { ...coreValidator.email, optional: true },
  //   description: { ...coreValidator.stringDefault, optional: true },
  //   countryId: coreValidator.modelIdInBody(countryRepository.modelName),
  //   imgId: { ...coreValidator.fileIdInBody, optional: true },
  // }),

  // update: checkSchema({
  //   id: coreValidator.modelIdInParams(repository.modelName),
  //   name: { ...coreValidator.name, optional: true },
  //   shortName: { ...coreValidator.name, optional: { options: { nullable: true } } },
  //   address: { ...coreValidator.stringDefault, optional: { options: { nullable: true } } },
  //   phone: { ...coreValidator.phone, optional: { options: { nullable: true } } },
  //   email: { ...coreValidator.email, optional: true },
  //   description: { ...coreValidator.stringDefault, optional: { options: { nullable: true } } },
  //   countryId: { ...coreValidator.modelIdInBody(countryRepository.modelName), optional: true },
  //   imgId: { ...coreValidator.fileIdInBody, optional: true },
  // }),

  isExist: checkSchema({
    id: coreValidator.modelIdInParams(repository.modelName),
  }),

  isDeleted: checkSchema({
    id: coreValidator.isNotDeletedResource(repository.modelName),
  }),

  getAll: checkSchema({
    offset: { ...coreValidator.offset, optional: true },
    limit: { ...coreValidator.limit, optional: true },
    name: { ...coreValidator.nameInQuery, optional: true },
    include: { ...validationFields.include, optional: true },
    exclude: { ...validationFields.exclude, optional: true },
  }),
});
