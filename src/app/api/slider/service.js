import coreService from '../../core/service';
import repository from './repository';
import models from '../../models';
import { ServiceError } from '../../utils/error_handler';

const service = Object.create(coreService);

export default Object.assign(service, {
  /**
   *  Get All available resources
   *  @return {Array} array of resource ORM objects
   */
  getAll(language) {
    try {
      const options = {
        order: [
          ['position', 'DESC'],
        ],
        attributes: [
          'name',
          [`title_${language}`, 'title'],
          'url',
          'type',
        ],
        include: [
          {
            model: models.file,
            attributes: ['id', 'path', 'name', 'extension', 'dimensions'],
          },
        ],
      };
      return repository.findAll({}, options);
    } catch (error) {
      throw new ServiceError(error);
    }
  },
});
