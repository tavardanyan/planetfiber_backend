import * as controller from './controller';
import validator from './validator';

export default (router) => {
  // ----------------------------------------------------------------------------------------------
  router.get('/', validator.get, controller.get);
  // ----------------------------------------------------------------------------------------------
  router.post('/', validator.create, controller.create);
  // ----------------------------------------------------------------------------------------------
};
