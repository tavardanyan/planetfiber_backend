import service from './service';
import cw from '../../core/controller';

export const get = cw((req) => service.get(req.language));

export const create = cw((req) => service.create(req.body));
