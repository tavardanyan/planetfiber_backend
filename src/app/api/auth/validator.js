import { checkSchema } from 'express-validator/check';

import { coreValidator, errorMessage, regexValidation } from '../../core/validator';

const validator = Object.create(coreValidator);

const validationFields = {
  username: {
    trim: true,
    in: ['body'],
    isString: {
      errorMessage: errorMessage.isString,
      negated: false,
    },
    isLength: {
      errorMessage: errorMessage.fieldFromToString(2, 20),
      options: { min: 2, max: 20 },
    },
    custom: {
      errorMessage: errorMessage.userNameOrEmailRequired,
      async options(username, req) {
        if (username && req.req.body.email) return false;
        if (!username && !req.req.body.email) return false;
        return true;
      },
    },
  },
  email: {
    ...coreValidator.stringDefault,
    isEmail: true,
    custom: {
      errorMessage: errorMessage.userNameOrEmailRequired,
      async options(email, req) {
        if (email && req.req.body.username) return false;
        if (!email && !req.req.body.username) return false;
        return true;
      },
    },
  },
  password: {
    in: ['body'],
    trim: true,
    errorMessage: errorMessage.fieldRequired,
    isEmpty: {
      negated: true,
    },
    isLength: {
      errorMessage: errorMessage.fieldFromToString(6, 255),
      options: { min: 6, max: 255 },
    },
  },

  deviceToken: {
    ...coreValidator.stringDefault,
    matches: {
      options: [/[0-9a-zA-Z\-\\_]/],
      errorMessage: errorMessage.isTokenRegex,
    },
  },
  devicePlatform: {
    in: ['body'],
    matches: {
      options: [regexValidation.platform],
      errorMessage: errorMessage.fieldMustBeOneOf(regexValidation.platform),
    },
  },

};

export default Object.assign(validator, {
  logIn: checkSchema({
    username: { ...validationFields.username, optional: true },
    email: { ...validationFields.email, optional: true },
    password: validationFields.password,
  }),

  addToken: checkSchema({
    deviceToken: validationFields.deviceToken,
    devicePlatform: validationFields.devicePlatform,
  }),
});
