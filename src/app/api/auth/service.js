import uuid from 'uuid/v4';
import { get as redisGet } from '../../utils/redis';
import coreService from '../../core/service';
import repository from './repository';
import { ServiceError, AuthError } from '../../utils/error_handler';
import { verify } from '../../utils/crypto_util';
import { generateAccessToken } from '../../utils/JWT_util';

const service = Object.create(coreService);

export default Object.assign(service, {
  /**
   *  Add token
   *  @param {Object} data : resource data object
   *  @return {Object} resource ORM object
   */
  async addToken(userId, data) {
    try {
      const { devicePlatform: platform, deviceToken: token } = data;
      await repository.create(
        {
          userId,
          platform,
          token,
        },
        {},
        'firebaseToken'
      );
      return;
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Login user
   *  @param {Object} data : resource data object
   *  @return {Object} resource ORM object
   */
  async logIn(data) {
    try {
      let user = await repository.findUser(data);

      if (!user) {
        throw new AuthError('Wrong credentials');
      }

      const verifies = verify(data.password, user.password, user.id);
      if (!verifies) {
        throw new AuthError('Wrong credentials');
      }

      user = await repository.findByPk(user.id, {
        include: ['department', 'role'],
      });

      const token = generateAccessToken(user.id);

      let redisData = await redisGet(user.id);

      if (!redisData) {
        await global.redisClient.set(user.id, JSON.stringify({ user }));
        redisData = await redisGet(user.id);
      }
      const sessionId = uuid();

      redisData.user = user;
      if (redisData.sessions) {
        redisData.sessions.push({ sessionId, token });
      } else redisData.sessions = [{ sessionId, token }];

      global.redisClient.set(user.id, JSON.stringify(redisData));

      return { token, sessionId };
    } catch (error) {
      throw new ServiceError(error);
    }
  },
});
