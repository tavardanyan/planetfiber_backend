import service from './service';
import cw from '../../core/controller';

export const getAll = cw((req) => service.getAll(req.language));

export const get = cw((req) => service.getByPk(req.params.id, req.language));
