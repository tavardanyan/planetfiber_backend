import coreService from '../../core/service';
import repository from './repository';
import { ServiceError } from '../../utils/error_handler';

const service = Object.create(coreService);

export default Object.assign(service, {
  /**
   *  Get All available resources
   *  @return {Array} array of resource ORM objects
   */
  getAll(language) {
    try {
      const options = {
        attributes: [
          'id',
          [`name_${language}`, 'name'],
          [`description_${language}`, 'description'],
          'email',
          'address',
          'phone',
        ],
      };
      return repository.findAll({}, options);
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Get One available resource
   *  @param {Number} pk : resource pk
   *  @return {Object} resource ORM object
   */
  getByPk(pk, language) {
    try {
      const options = {
        attributes: [
          'id',
          [`name_${language}`, 'name'],
          [`description_${language}`, 'description'],
          'email',
          'address',
          'phone',
        ],
      };
      return repository.findByPk(pk, options);
    } catch (error) {
      throw new ServiceError(error);
    }
  },
});
