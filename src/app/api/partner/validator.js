import { checkSchema } from 'express-validator/check';
import { coreValidator, errorMessage } from '../../core/validator';
import repository from './repository';

const validator = Object.create(coreValidator);

const validationFields = {
  include: {
    ...coreValidator.queryString,
    custom: {
      errorMessage: errorMessage.includeExclude,
      options: (_include, { req }) => req.query.exclude,
    },
  },
  exclude: {
    ...coreValidator.queryString,
    custom: {
      errorMessage: errorMessage.includeExclude,
      options: (_exclude, { req }) => req.query.include,
    },
  },
};

export default Object.assign(validator, {

  isExist: checkSchema({
    id: coreValidator.modelIdInParams(repository.modelName),
  }),

  isDeleted: checkSchema({
    id: coreValidator.isNotDeletedResource(repository.modelName),
  }),

  getAll: checkSchema({
    include: { ...validationFields.include, optional: true },
    exclude: { ...validationFields.exclude, optional: true },
  }),
});
