import coreService from '../../core/service';
import repository from './repository';
import { ServiceError } from '../../utils/error_handler';
import models from '../../models';

const service = Object.create(coreService);

export default Object.assign(service, {
  /**
   *  Get All available resources
   *  @return {Array} array of resource ORM objects
   */
  async getAll(query, language) {
    try {
      const where = {};
      const options = {
        attributes: ['id', [`title_${language}`, 'title']],
        include: [
          {
            model: models.product,
            attributes: [
              'id',
              [`title_${language}`, 'title'],
              'key',
              'price',
              'availability',
            ],
            include: [
              {
                model: models.file,
                attributes: [
                  'id',
                  'path',
                  'name',
                  'extension',
                  'dimensions',
                  'isMain',
                ],
                order: [['isMain', 'DESC']],
                limit: 1,
              },
            ],
          },
        ],
        limit: query.limit || 20,
      };

      return repository.findAll(where, options);
    } catch (error) {
      throw new ServiceError(error);
    }
  },

  /**
   *  Get One available resources
   *  @param {Number} pk : resource ID
   *  @return {Object} resource ORM object
   */
  getByPk(pk, language) {
    try {
      return repository.findByPk(pk, {
        attributes: ['id', [`title_${language}`, 'title']],
        include: [
          {
            model: models.product,
            attributes: [
              'id',
              [`title_${language}`, 'title'],
              'key',
              'price',
              'availability',
            ],
            include: [
              {
                model: models.file,
                attributes: [
                  'id',
                  'path',
                  'name',
                  'extension',
                  'dimensions',
                  'isMain',
                ],
                order: [['isMain', 'DESC']],
                limit: 1,
              },
            ],
          },
        ],
      });
    } catch (error) {
      throw new ServiceError(error);
    }
  },
});
