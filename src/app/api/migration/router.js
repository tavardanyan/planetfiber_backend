import * as controller from './controller';
import validator from './validator';

export default (router) => {
  // ----------------------------------------------------------------------------------------------
  router.get('/nur/etargim/', validator.migrateRun, controller.migrateRun);
  // ----------------------------------------------------------------------------------------------
  router.get('/odnu/etargim/', validator.migrateUndo, controller.migrateUndo);
  // ----------------------------------------------------------------------------------------------
};
