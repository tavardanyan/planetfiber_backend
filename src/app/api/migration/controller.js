import service from './service';
import cw from '../../core/controller';

export const migrateRun = cw((req) => service.migrateRun(req.query));

export const migrateUndo = cw((req) => service.migrateUndo(req.query));
