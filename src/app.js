/* eslint-disable class-methods-use-this */
import fs from 'fs';
import path from 'path';
import express from 'express';
import expressValidator from 'express-validator';
import monitoring from 'express-status-monitor';
import logger from 'morgan';
import Sequelize from 'sequelize';
import redis from 'redis';
import fileUpload from 'express-fileupload';
import swaggerUi from 'swagger-ui-express';

import { setUpDb } from './app/models';
import enableRoutes from './app/';
import sequelizeConfig from '../sequelizeConfig';
import { ErrorHandler } from './app/utils/error_handler';
import SwaggerConfig from '../swaggerConfig';
import MonitoringConfigs from '../monitoringConfig';
import { ISO_LANGS_OBJECT } from './data/TYPES';
import appendLanguage from './app/utils/append_language';
import { slackRequest } from './app/utils/slack';
import { dataInsertion } from './seeders';

class Application {
  constructor() {
    this.app = express();
    this.app.use(monitoring(MonitoringConfigs));
    this.initApp();
  }

  async initApp() {
    await this.configApp();
    await this.configLogger();
    await this.dbConfig();
    await this.dbRedisConfig();
    await this.setParams();
    await this.setFileParser();
    await this.isDbTableCreated();
    await dataInsertion();
    await this.getLanguages();
    await this.setRouter();
    await this.setErrorHandler();
    await this.set404Handler();
  }

  async isDbTableCreated() {
    await new Promise((res) => {
      const interval = setInterval(async () => {
        try {
          const dbDetect = await this.db.sequelize.query(`
          SELECT table_name FROM information_schema.tables 
          WHERE  table_schema = 'public';`);
          const seedItems = [
            'countries',
            'custom_pages',
            'users',
            'categories',
          ];
          let isSeedTableExists = true;
          for (const seedItem of seedItems) {
            const founded = dbDetect.find((dbTable) => dbTable[0] === seedItem);
            if (!founded) {
              isSeedTableExists = false;
            }
          }
          if (isSeedTableExists) {
            clearInterval(interval);
            res();
          }
        } catch (e) {
          throw new Error(e);
        }
      }, 1000);
    });
  }

  async configApp() {
    this.app.use(
      express.json({
        limit: '5mb',
      })
    );
    this.app.use(
      express.urlencoded({
        limit: '5mb',
        extended: true,
      })
    );
    this.app.use(expressValidator());
    this.app.use((req, res, next) => {
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Access-Control-Allow-Credentials', true);
      res.setHeader(
        'Access-Control-Allow-Methods',
        'GET,POST,PUT,PATCH,DELETE'
      );
      res.setHeader(
        'Access-Control-Allow-Headers',
        'Content-Type, Authorization, x-app-session'
      );
      if (req.method === 'OPTIONS') {
        return res.json();
      }
      return next();
    });
    if (process.NODE_ENV !== 'production') {
      this.app.use(
        '/api/assets',
        express.static(path.join(__dirname, '../', 'public'))
      );
    }
  }

  async configLogger() {
    const logFilePath = path.join(__dirname, '../', 'access.log');
    const accessLogStream = fs.createWriteStream(logFilePath, { flags: 'a' });
    this.app.use(logger('combined', { stream: accessLogStream }));
    this.app.use('/logger', (_req, res) => res.sendFile(logFilePath));
  }

  async dbConfig() {
    const sequelize = new Sequelize(sequelizeConfig);
    this.db = await setUpDb(sequelize);
  }

  async dbRedisConfig() {
    const client = await redis.createClient({
      port: process.env.REDIS_PORT,
      host: process.env.REDIS_HOST,
    });
    client.on('error', (error) => {
      throw new Error(`Error REDIS ${error}`);
    });
    client.on('connect', () => {
      console.log('REDIS connect ready');
    });
    global.redisClient = client;
  }

  async getLanguages() {
    try {
      Object.keys(ISO_LANGS_OBJECT).forEach(async (key) => {
        await global.redisClient.set(
          key,
          JSON.stringify({ ...ISO_LANGS_OBJECT[key], code: key })
        );
      });
    } catch (error) {
      throw new Error(error);
    }
  }

  async setParams() {
    this.app.set('env', process.env.NODE_ENV);

    // eslint-disable-next-line no-extend-native
    BigInt.prototype.toJSON = function toJSON() {
      return this.toString();
    };
  }

  async setFileParser() {
    global.uploadFolder = path.join(__dirname, '..', process.env.UPLOAD_FOLDER);
    if (!fs.existsSync(global.uploadFolder)) {
      fs.mkdirSync(global.uploadFolder);
    }
    this.app.use(
      fileUpload({
        safeFileNames: /\\/g,
        preserveExtension: 0,
        createParentPath: true,
      })
    );
    this.app.use(express.static(process.env.UPLOAD_FOLDER));
  }

  async setRouter() {
    this.app.use(
      '/api-docs',
      swaggerUi.serve,
      swaggerUi.setup(SwaggerConfig())
    );
    this.router = express.Router();
    this.app.use('/', appendLanguage);
    this.app.use('/', await enableRoutes(this.router));
    this.app.use('/', (_req, res) => res.send('WOW!!!  Project is running!'));
  }

  async set404Handler() {
    this.app.use((_req, res) => {
      res.status(404).json({
        status: 'Error',
        message: '',
        data: null,
        errors: '',
      });
    });
  }

  async setErrorHandler() {
    // eslint-disable-next-line no-unused-vars
    this.app.use((error, _req, res, _next) => {
      if (error.formatWith && typeof error.formatWith === 'function') {
        return res.status(422).json({
          status: 'Error',
          message: error.message,
          data: null,
          errors: error.mapped(),
        });
      }
      // log in error_log file
      console.error(error.stack);
      if (error instanceof ErrorHandler) {
        return res.status(error.status || 400).json({
          status: error.name,
          message: error.message,
          data: null,
          errors: error.errors,
        });
      }
      slackRequest('500-error', `\`\`\` ${JSON.stringify(error)} \`\`\``);
      return res.status(500).json({
        status: 'Error',
        message: error.message,
        data: null,
        errors: null,
      });
    });
  }
}

const instanceOfApplication = new Application();
export default () => instanceOfApplication.app;
