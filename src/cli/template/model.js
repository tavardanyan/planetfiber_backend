module.exports = (sequelize, DataTypes) => {
  const model = sequelize.define('%%modelName%%', {
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: '%%tableName%%',
    underscored: true,
    paranoid: true,
    defaultScope: {
      attributes: { exclude: ['updatedAt', 'createdAt', 'deletedAt'] },
    },
    hooks: {
      afterSave: async (n) => {
        // eslint-disable-next-line no-param-reassign
        if (n && n.dataValues) ['createdAt', 'updatedAt', 'deletedAt'].forEach((prop) => delete n.dataValues[prop]);
      },
    },
  });
  return model;
};
