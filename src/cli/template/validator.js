import { checkSchema } from 'express-validator/check';

import models from '../../models';
import { coreValidator, errorMessage } from '../../core/validator';

const validator = Object.create(coreValidator);

export default Object.assign(validator, {
  store: checkSchema({
  }),

  upsert: checkSchema({
  }),

  update: checkSchema({
  }),

  isExist: checkSchema({
  }),

  isDeleted: checkSchema({
  }),

  getAll: checkSchema({
  }),
});
