/* eslint-disable import/prefer-default-export */
import models from '../app/models';
import fs from 'fs';
import path from 'path';

const uuid = require('uuid/v4');
const crypto = require('crypto');
const usersJson = require('./user.json');
const countriesJson = require('./country.json');
const contactsJson = require('./contact.json');
const partnersJson = require('./partner.json');
const categoriesJson = require('./category.json');
const attributesJson = require('./attribute.json');
const productsJson = require('./product.json');
const slidersJson = require('./slider.json');
const sectionsJson = require('./section.json');
const customPagesJson = require('./custom_page.json');

const salt = process.env.PASSWORD_SALT;

async function createCountries() {
  const countries = await models.country.findAll({
    attributes: ['id'],
    limit: 5,
  });
  if (countries.length === 0) {
    await models.country.bulkCreate(countriesJson);
  }
}

async function createContact() {
  const contacts = await models.contact.findAll({
    attributes: ['id'],
    limit: 5,
  });
  if (contacts.length === 0) {
    await models.contact.bulkCreate(contactsJson);
  }
}

async function createCustomPages() {
  const customPages = await models.custom_page.findAll({
    attributes: ['name'],
  });
  if (customPages.length === 0) {
    await models.custom_page.bulkCreate(customPagesJson);
  }
}

async function createUsers() {
  const users = await models.user.findOne({ where: { username: 'admin' } });
  if (!users) {
    usersJson.forEach((item) => {
      const userId = uuid();
      item.id = userId;
      item.password = crypto
        .scryptSync(item.password, `${userId}${salt}`, 64)
        .toString('hex');
    });
    await models.user.bulkCreate(usersJson);
  }
}

async function createCategories() {
  const categories = await models.category.findAll({
    attributes: ['id'],
    limit: 5,
  });
  if (categories.length === 0) {
    await models.category.bulkCreate(categoriesJson);
  }
}

async function createPartners() {
  const partners = await models.partner.findAll({
    attributes: ['id'],
    limit: 5,
  });
  if (partners.length === 0) {
    await models.partner.bulkCreate(partnersJson);
  }
}

async function createAttributes() {
  const attributes = await models.attribute.findAll({
    attributes: ['id'],
    limit: 5,
  });
  if (attributes.length === 0) {
    for await (let attributeObj of attributesJson) {
      const categoryIds = [];
      const values = {};
      if (attributeObj.type === 'number') {
        values.min = attributeObj.values.split('---')[0].trim();
        values.max = attributeObj.values.split('---')[1].trim();
      } else {
        values.values = [];
        attributeObj.values.split(';').forEach((value) => {
          values.values.push(value.trim());
        });
      }
      attributeObj.category_keys.split(';').forEach((key) => {
        categoryIds.push(
          categoriesJson.find((category) => category.key === key.trim()).id
        );
      });
      attributeObj.values = values;
      const attrCreated = await models.attribute.create(attributeObj);
      await attrCreated.addCategories(categoryIds);
    }
  }
}

async function createProducts() {
  const products = await models.product.findAll({
    attributes: ['id'],
    limit: 5,
  });
  if (products.length !== 0) return;

  for await (let productObj of productsJson) {
    const categoryKeys = productObj.key.split('-').slice(0, -1);
    const categoryIds = [];
    categoryKeys.forEach((key) => {
      const id = categoriesJson.find(
        (category) => category.key === key.trim()
      )?.id;
      if (id) categoryIds.push(id);
    });

    const attributes = [];
    Object.keys(productObj).forEach((key) => {
      if (
        key.startsWith('attribute_id') &&
        typeof productObj[key] === 'number'
      ) {
        const obj = {};
        const value =
          productObj[`attribute_value_${key.split('_').pop()}`].toString();
        if (value.includes('---')) {
          obj.min = value.split('---')[0].trim();
          obj.max = value.split('---')[1].trim();
        } else {
          obj.value = value;
        }
        attributes.push({ id: productObj[key], value: obj });
      }
    });
    const prodCreated = await models.product.create(productObj);
    await prodCreated.addCategories(categoryIds);
    for await (let attr of attributes) {
      await prodCreated.addAttribute(attr.id, {
        through: { value: attr.value },
      });
    }
  }
}

async function createFiles() {
  const files = await models.file.findAll({
    attributes: ['id'],
    limit: 5,
  });
  if (files.length !== 0) return;
  const uploadFolder = path.join(__dirname, '../..', process.env.UPLOAD_FOLDER);
  const folders = fs.readdirSync(uploadFolder);

  for await (let folder of folders) {
    const folderData = {
      id: uuid(),
      name: folder,
      path: '',
      type: 'FOLDER',
    };

    await models.file.create(folderData);

    const fileNames = fs.readdirSync(path.join(uploadFolder, folder));
    let count = 0;
    for await (let filename of fileNames) {
      const stat = fs.statSync(path.join(uploadFolder, folder, filename));

      const insertData = {
        id: uuid(),
        name: path.parse(filename).name,
        path: folder,
        isMain: count === 0,
        size: stat.size / 1000,
        extension: path.extname(filename),
        parentId: folderData.id,
        productId: productsJson.find((product) => product.key === folder)?.id,
      };

      await models.file.create(insertData);
      count++;
    }
  }
}

async function createSlider() {
  const sliders = await models.slider.findAll({
    attributes: ['name'],
    limit: 5,
  });
  if (sliders.length === 0) {
    const files = await models.file.findAll({
      where: { type: 'FILE' },
      limit: 4,
      offset: 305,
    });
    slidersJson.forEach((slider, index) => {
      slider.fileId = files[index].id;
    });
    await models.slider.bulkCreate(slidersJson);
  }
}

async function createSections() {
  const sections = await models.section.findAll({
    attributes: ['id'],
    limit: 5,
  });
  if (sections.length === 0) {
    let count = 8;
    sectionsJson.forEach(async (section) => {
      const products = await models.product.findAll({
        offset: count * 40,
        limit: 40,
      });
      const productIds = products.map((prod) => prod.id);
      console.log(productIds);
      const sectionCreated = await models.section.create(section);
      await sectionCreated.addProducts(productIds);
    });
  }
}

export async function dataInsertion() {
  try {
    await createCountries();
    await createContact();
    await createPartners();
    await createUsers();
    await createCustomPages();
    await createCategories();
    await createAttributes();
    await createProducts();
    await createFiles();
    await createSlider();
    await createSections();

    console.log('The seeds had already been ran');
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
}
